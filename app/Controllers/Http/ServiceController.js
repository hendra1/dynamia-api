'use strict'

const Database = use('Database')

class ServiceController {
    async index({request, params, response}) {
      const tableName = params.table
      return await Database.table(tableName).select('*')
    }

    async store({request, params, response}) {
      try {
        const tableName = params.table
        const data = request.all()
        const id = await Database.insert(data).into(tableName);
        return response.json({status: 'success', message: id})
      } catch (e) {
        return response.status(500).json({status: 'false', message: e.message})
      }
    }

    async update({request, params, response}) {
      const tableName = params.table
      const data = request.all()
      const result = await Database.table(tableName).where("ID",).update(data)
    }
}

module.exports = ServiceController
